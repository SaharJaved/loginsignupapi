var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/helloworld', function(req, res) {
    res.render('helloworld', { title: 'Hello, World!' });
});

router.get('/userlist', function(req, res) {
    var db = req.db;
    var collection = db.get('usercollection');
    collection.find({},{},function(e,docs){
        res.send( {
            "userlist" : docs
        });
    });
});

router.post('/adduser', function(req, res) {

  
    var db = req.db;

    var username = req.body.username;
    var useremail = req.body.useremail;
  var collection = db.get('usercollection');

  
    collection.insert({
        "username" : username,
        "email" : useremail
    }, function (err, doc) {
        if (err) {
         
            res.send("There was a problem ");
        }
        else {
            res.redirect("userlist");
        }
    });
});

router.post('/login', function(req, res) {
    var username = req.body.username;
    var useremail = req.body.useremail;

  
    var db = req.db;
    var collection = db.get('usercollection');
    collection.findOne({username:username},{useremail:useremail},function(e,user){
       if(collection) {
          
        res.send("Ok.");
    }else
      console.log(res)
        res.send("eror.");
    
   });
    });

module.exports = router;
